import Vue from 'vue'
import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/es'
import '@/assets/scss/app.scss'
import 'element-ui/lib/theme-chalk/base.css'

Vue.use(Element, { locale })
